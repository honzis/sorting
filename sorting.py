import pygame
import random
import time
pygame.init()

def main():
    def draw(surface, array):
        surface_size = surface.get_size()
        space = round((surface_size[0] / len(array)) / 5)
        block_size = [((surface_size[0] - ((len(array) - 1) * space)) / len(array)), (surface_size[1] / len(array))]
        for i, value in enumerate(array):
            rect = pygame.Rect(0, 0, block_size[0], block_size[1] * value)
            rect.bottomleft = ((i * block_size[0]) + (space * i), surface_size[1])
            pygame.draw.rect(surface, "white", rect)
            # pygame.draw.rect(surface, f"gray{int(value / (len(array) / 100))}", rect)

    def bubble_sort(array):
        yield array
        swapped = True
        cycles = 0
        while swapped:
            swapped = False
            for i in range((len(array) - 1) - cycles):
                if array[i] > array[i + 1]:
                    array[i], array[i + 1] = array[i + 1], array[i]
                    swapped = True
            yield array
            cycles += 1

    def selection_sort(array):
        yield array
        for i in range(len(array) - 1):
            i2 = array.index(min(array[i:]))
            array[i], array[i2] = array[i2], array[i]
            yield array

    def insertion_sort(array):
        yield array
        for i in range(len(array)):
            key = array[i]
            for j in range(i - 1, -1, -1):
                if array[j] > key:
                    array[j + 1] = array[j]
                    array[j] = key
                else:
                    break
                yield array

    algorithms = (bubble_sort, selection_sort, insertion_sort)

    while True:
        try:
            array_length = int(input("Length of array >"))
            if array_length == 0:
                continue
            print()
            break
        except ValueError:
            continue

    array = [n for n in (range(1, array_length + 1))]
    random.shuffle(array)
    array_sorted = False

    while True:
        try:
            for i, algorithm in enumerate(algorithms):
                print(f"{i + 1}. {algorithm.__name__.replace('_', ' ').capitalize()}")
            algorithm_choice = int(input("Sorting algorithm >"))
            if algorithm_choice not in range(1, len(algorithms) + 1):
                continue
            print()
            break
        except ValueError:
            continue

    algorithm = algorithms[algorithm_choice - 1]

    generator = algorithm(array)
    timer = 0

    window_size = [2000, 1000]
    if window_size[0] < len(array):
        window_size[0] = len(array)
    window = pygame.display.set_mode(window_size)

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                if event.key == pygame.K_r:
                    array = [n for n in (range(1, array_length + 1))]
                    random.shuffle(array)
                    array_sorted = False
                    generator = algorithm(array)
                    timer = 0

        if not array_sorted:
            try:
                start = time.perf_counter()
                array = next(generator)
                end = time.perf_counter()
                timer += end - start

            except StopIteration:
                print(f"Sorted in: {(round(timer, 4))}s")
                array_sorted = True
            
            finally:
                window.fill("black")
                draw(window, array)
                pygame.display.set_caption(f"{algorithm.__name__.replace('_', ' ').capitalize()} - {len(array)} values, {(round(timer, 4))} elapsed")
                pygame.display.flip()

    pygame.quit()

if __name__ == "__main__":
    main()
